class RemovePasswordField < ActiveRecord::Migration
  
  #we will remove this becouse we will use encrypted password that cmes with devise
  def change
  	remove_column :users, :password
  end
end
