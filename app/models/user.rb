class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << (options[:message] || "not valid")
    end
  end
end


class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable , :validatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

	has_many :tweets

  has_many :followings_relationship, :dependent => :destroy, :class_name => 'Relationship', :foreign_key => :follower_id
  has_many :followings, :through => :followings_relationship, :source => :followed

  has_many :followers_relationship, :dependent => :destroy, :class_name => 'Relationship', :foreign_key => :followed_id
  has_many :followers, :through => :followers_relationship, :source => :follower

	validates :name, :username, :password, :presence => true, length: { minimum: 1, maximum: 40 }
	validates :username, :email, uniqueness: { case_sensitive: false }
	validates :email, email: true


  validates :username, format: { with:/\A[a-zA-Z0-9_]*[a-zA-Z][a-zA-Z0-9_]*\z/}


  def timeline

    ##joins(:followers_relationship, :tweets).where("relationships.follower_id = ? or users.id = ?", user_id, user_id).select("users.*, tweets.message")
    #joins('RIGHT JOIN relationships ON relationships.followed_id = users.id RIGHT JOIN tweets ON tweets.user_id = relationships.followed_id')
    #.where("relationships.follower_id = ? or tweets.user_id = ?", user_id, user_id)
    #.select("users.*, tweets.message, tweets.created_at as t_created_at")
    #.order('tweets.created_at DESC')
    User.find_by_sql("SELECT users.*, tweets.message, tweets.created_at as t_created_at, tweets.user_id
                      FROM `users` JOIN relationships ON relationships.followed_id = users.id JOIN tweets ON tweets.user_id = relationships.followed_id
                      WHERE (relationships.follower_id = #{id})  
                      UNION
                      SELECT users.*, tweets.message, tweets.created_at as t_created_at, tweets.user_id
                      FROM `users` JOIN tweets ON tweets.user_id = users.id 
                      WHERE users.id = #{id}
                      ORDER BY t_created_at DESC")
  end


  def is_following(user_id)
    followings_relationship.find_by_followed_id user_id
  end

  def not_following
    User.where.not(id: followings).limit(5)
  end

end
