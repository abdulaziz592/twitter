class Tweet < ActiveRecord::Base

	belongs_to :user


	validate :valid_message_length
	validates_presence_of :user_id

	def valid_message_length
		message.strip!
		if message.length < 1
			errors.add(:message, 'should be at least one letter')
		elsif message.length > 140
			errors.add(:message, 'should not be more than 140 letters')
		end
	end

end
