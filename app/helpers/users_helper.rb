module UsersHelper
	def list_users_for(users)
		render :partial => 'list_users', :locals => {:users => users}
	end
end
