module ApplicationHelper

	def error_messages_for(object)
	    render(:partial => 'application/error_messages',
	      :locals => {:object => object})
	end

	def mini_profile_for(user)
		render :partial => 'application/mini_user_profile', :locals => {:user => user}
	end

	def print_tweet(t)
		render :partial => 'application/tweet', :locals => {:t => t}
	end
	
end
