class RelationshipsController < ApplicationController

	before_action :authenticate_user!

	def create
		relationship = current_user.followings_relationship.build(:followed_id => params[:user_id])
		if relationship.save
			flash[:notice] = 'User followed'
			redirect_to :back
		else
			flash[:error] = 'Can\'t follow user'
			redirect_to :back
		end
		
	end

	def destroy
		relationship = current_user.followings_relationship.find_by_followed_id(params[:user_id])
		relationship.destroy
		flash[:notice] = 'User unfollowed'
		redirect_to :back
	end


	def params_for_following
		params.require(:user).permit(:user_id)
	end

end
