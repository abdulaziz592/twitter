class TweetsController < ApplicationController

	before_action :authenticate_user!

	def index
		@tweets = Tweet.all
	end

	def new

	end

	def create
		@tweet = Tweet.new valid_tweet_params
		@tweet.user_id = current_user.id
		if @tweet.save
			flash[:notice] = 'tweet created successflly'
			redirect_to :back
		else
			render 'new'
		end
	end

	def search
		@tweets = Tweet.joins(:user).where('message like ?', "%#{params[:hashtag]}%").select("users.*, tweets.message, tweets.created_at as t_created_at").order('tweets.created_at DESC')
	end

	def valid_tweet_params
    	params.require(:tweet).permit(:message)
  	end

end
