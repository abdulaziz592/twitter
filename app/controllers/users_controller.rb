class UsersController < ApplicationController

  before_action :authenticate_user!

  def index
    @users = User.all
  end

  def show
    @user = User.find_by_username(params[:username])
    if @user.blank?
      not_found
    end
  end

  def timeline
  	@tm = current_user.timeline
  end

  def list
  	if params[:s] == 'followers'
  		@users = current_user.followers
  	elsif params[:s] == 'followings'
  		@users = current_user.followings
  	else
  		not_found
  	end
  	render 'index'
  end

end
