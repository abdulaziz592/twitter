// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require_tree .


String.prototype.linkify_tweet = function() {
   var tweet = this.replace(/(^|\s)@(\w+)/g, "<a href=\"/$2\">$1@$2</a>");
   return tweet.replace(/(^|\s)#(\w+)/g, "<a href=\"/hashtag/$2\">$1#$2</a>");
 };

$(function() {
  $('.tweet').each(function (i, el) {
  	var t = $(el).text().linkify_tweet();
  	$(el).html(t);
  });
});